var fs = require("fs");
var path = require('path')

var multer = require("multer");
var fastCsv = require("fast-csv");
var PDFDocument = require('pdfkit');

var upload = multer({ dest: "src/media/" });

const uploadController = {
    processCSVAndRetrievePDF: function(req, res) {
        if (!req.file || Object.keys(req.file).length === 0) {
            return res.status(400).send("No files were uploaded.");
        }

        let arquivoRecebido = req.file;
        let parsedFile = [];

        const recebimentoDoArquivo = new Promise((resolve) => {
            fs.createReadStream(arquivoRecebido.path, { encoding: "utf-8" })
                .pipe(
                    fastCsv.parse({
                        headers: [
                            "data",
                            "hora",
                            "ano",
                            "",
                            "titulo_eleicao",
                            "estado",
                            "",
                            "cod_cidade",
                            "cidade",
                            "",
                            "",
                            "nro_candidato",
                            "",
                            "",
                            "nome_candidato",
                            "cargo_disputado",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "resultado",
                            "nro_partido_politico",
                            "sigla_partido_politico",
                            "nome_partido_politico",
                            "",
                            "coligacao",
                            "",
                            "",
                        ],
                        delimiter: ";",
                        encoding: "utf-8",
                    })
                )
                .on("error", (error) => console.error(error))
                .on("data", (row) => parsedFile.push(row))
                .on("end", (rowCount) => console.log(`Parsed ${rowCount} rows`))
                .on("end", () => resolve());
        });

        recebimentoDoArquivo.then(() => {

            const filtered = parsedFile.filter(candidato => {
                return (candidato.cod_cidade === '93734'
                    && candidato.cargo_disputado === 'DEPUTADO ESTADUAL'
                    && candidato.resultado === 'ELEITO')
            })

            const unique = [...new Set(filtered.map(candidato => `${candidato['nro_candidato']}      -     ${candidato['nome_candidato']}     -      ${candidato['sigla_partido_politico']}`))];

            const doc = new PDFDocument({ bufferPages: true });
            let buffers = []
            doc.on('data', buffers.push.bind(buffers))
            doc.on('end', () => {

                let pdfData = Buffer.concat(buffers)

                res.writeHead(200, {
                    'Content-Length': Buffer.byteLength(pdfData),
                    'Content-Type': 'application/pdf',
                    'Content-disposition': 'attachment;filename=test.pdf',})
                    .end(pdfData);

            })

            doc.font('Helvetica')
                .fontSize(24)

            doc.text('Deputados Estaduais eleitos por Goiânia');
            doc.moveDown();

            doc.fontSize(12)

            doc.text(`Nro          -     Nome Candidato       -    Partido`);
            doc.moveDown();

            unique.forEach(candidato => {
                doc.text(`${candidato}`);
                doc.moveDown();
            });

            doc.end();
        })
    }
}

module.exports = uploadController;