var express = require("express");
var router = express.Router();

var multer = require("multer");

var uploadController = require('../controllers/uploadController');

var upload = multer({ dest: "src/media/" });

router.get("/", function (req, res) {
  return res.render('index')
});

router.post("/upload", upload.single("arquivo"), uploadController.processCSVAndRetrievePDF);

module.exports = router;
