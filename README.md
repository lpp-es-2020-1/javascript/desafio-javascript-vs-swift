## Titulo: Rest da Comunidade.

### Conceitos a provar:

Meu objetivo é provar que é possível construir aplicações Javascript para diferentes fins utilizando o máximo possível bibliotecas open source (bibliotecas da comunidade) utilizadas amplamente na comunidade da linguagem.

Métrica: Quantidade de bibliotecas utilizadas no projeto com mais de 350000 downloads semanais.
Neste projeto todas as bibliotecas se enquadram na métrica. Ou seja 8 bibliotecas utilizadas.

```json
{
    "cookie-parser": "~1.4.4",
    "debug": "~2.6.9",
    "ejs": "^3.1.5",
    "express": "~4.16.1",
    "fast-csv": "^4.3.6",
    "http-errors": "~1.6.3",
    "morgan": "~1.9.1",
    "pdfkit": "^0.11.0"
}
```

### O Desafio:

Desenvolver uma aplicação servidor REST que irá receber uma chamada HTTP POST contendo um arquivo CSV (dentro da pasta files, no repositório). Nesse arquivo deve contém os dados dos candidatos a eleição em 2010. Utilizando esse arquivo a aplicação irá ler os valores, descobrir quais foram os deputado estaduais eleitos em 2010 em Goiânia e gerar um .pdf contendo os dados desse politico.

Para tanto, deve utilizar somente bibliotecas da comunidade, tanto para ler o csv quanto para gerar o pdf.


### Wiki
Link para wiki da linguagem: [Aqui](https://gitlab.com/lpp-es-2020-1/javascript/wiki)
